export class LogModel {
    message: string = '';
    fields: object;
    level: LogLevel
}

export enum LogLevel {
    INFO = 'info',
    ERROR = 'error'
}

