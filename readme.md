# processo seletivo para ser um desenvolvedor da Mutant.

### Installation with node
Install the dependencies and devDependencies and start the server.



```sh
$ npm i 
```

### Test

Run unit test

```sh
$ npm test 
```


### Run

Run project

```sh
$ npm start 
```

### Installation with Docker


```sh
$  docker-compose up -d
```


### Routes

| Function | Route |
| ------ | ------ |
| Os websites de todos os usuários | /user/web-site |
| O Nome, email e a empresa em que trabalha (em ordem alfabética) | /user/name-email-company |
| Mostrar todos os usuários que no endereço contem a palavra '''suite''' | /stocks/filter-suite |


