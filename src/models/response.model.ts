export class ResponseModel {
    data?: any;
    error?: any;
    status: number = null;
    message: string = ''
}