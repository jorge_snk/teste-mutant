import Logger from '../middleware/logs/logger.middleware'
import { Request, Response } from 'express'
import { LogLevel } from '../models/log.model'
import UserService from '../services/user.service'

class UserController extends Logger {
    constructor() {
        super()
        this.getWebsites = this.getWebsites.bind(this)
        this.getNameEmailCompany = this.getNameEmailCompany.bind(this)
        this.filterSuite = this.filterSuite.bind(this)
    }

    async getWebsites(_request: Request, response: Response) {
        try {
            const websites = await UserService.getWebsites()
            this.createLog({ level: LogLevel.INFO, fields: { route: 'getWebsites' }, message: websites.message })
            response.status(websites.status).send(websites)

        } catch (e) {
            this.createLog({ fields: { route: 'getWebsites', ...e }, level: LogLevel.ERROR, message: e.message })
            response.status(e.status).send(e)
        }
    }


    async getNameEmailCompany(_request: Request, response: Response) {
        try {
            const websites = await UserService.getNameEmailCompany()
            this.createLog({ level: LogLevel.INFO, fields: { route: 'getNameEmailCompany' }, message: websites.message })
            response.status(websites.status).send(websites)

        } catch (e) {
            this.createLog({ fields: { route: 'getNameEmailCompany', errorInfo: e.error }, level: LogLevel.ERROR, message: e.message })
            response.status(e.status).send(e)
        }
    }

    async filterSuite(_request: Request, response: Response) {
        try {
            const websites = await UserService.filterSuite()
            this.createLog({ level: LogLevel.INFO, fields: { route: 'filterSuite' }, message: websites.message })
            response.status(websites.status).send(websites)

        } catch (e) {
            this.createLog({ fields: { route: 'filterSuite', errorInfo: e.error }, level: LogLevel.ERROR, message: e.message })
            response.status(e.status).send(e)
        }
    }


}

export default new UserController()