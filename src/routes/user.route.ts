import { Router } from 'express'
import UserController from '../controllers/user.controller'

const UserRouter = Router()

UserRouter.route('/web-site').get(UserController.getWebsites)
UserRouter.route('/name-email-company').get(UserController.getNameEmailCompany)
UserRouter.route('/filter-suite').get(UserController.filterSuite)

export default UserRouter