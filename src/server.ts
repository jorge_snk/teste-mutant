import Express from 'express'
import Constant from './constant'
import Logger from './middleware/logs/logger.middleware'
import { LogLevel } from './models/log.model'
import UserRouter from './routes/user.route'
class Server extends Logger {

    private App = Express()

    constructor() {
        super()
        this.middleware()
        this.routes()
        this.run()
    }



    middleware() {
        this.App.use(Express.json())
    }

    run() {
        this.App.listen(Constant.APP_PORT, () => {
            this.createLog({ fields: { data: "start app" }, message: `start app port:${Constant.APP_PORT}`, level: LogLevel.INFO })
        })
    }

    routes() {
        this.App.use('/user',UserRouter)
    }


}

export default new Server()