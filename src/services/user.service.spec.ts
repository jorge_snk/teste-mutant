import UserService from './user.service'
import Request from '../middleware/requests/requests.middleware'
describe('get websites', () => {

    it(' sucess find', async () => {
        const body = [
            { website: "teste.com" },
            { website: "teste1.com" },
            { website: "teste2.com" },
            { website: "test3.com" },
        ]

        const mockFunc = jest.fn();
        mockFunc.mockReturnValue(body);
        Request.getUsers = mockFunc.bind(Request)
        try {
            const response = await UserService.getWebsites()
            expect(response.status).toBe(200)
        } catch (e) {
            expect(e.status).toBe(200)
        }
    })

    it(' Error find', async () => {

        const mockFunc = jest.fn();
        mockFunc.mockReturnValue(Promise.reject({ status: 400, error: 'error' }));
        Request.getUsers = mockFunc.bind(Request)
        try {
            const response = await UserService.getWebsites()
            expect(response.status).not.toBe(200)
        } catch (e) {

            expect(e.status).toBe(400)
        }
    })
})



describe('get name email company', () => {

    it(' sucess find', async () => {
        const body = [
            { name: "teste4", email: 'teste@teste', company: { name: 'teste4' } },
            { name: "teste2", email: 'teste@teste', company: { name: 'teste2' } },

            { name: "teste1", email: 'teste@teste', company: { name: 'teste1' } },
            { name: "teste3", email: 'teste@teste', company: { name: 'teste3' } },

        ]

        const mockFunc = jest.fn();
        mockFunc.mockReturnValue(body);
        Request.getUsers = mockFunc.bind(Request)
        try {
            const response = await UserService.getNameEmailCompany()
            expect(response.status).toBe(200)
            expect(response.data[0].name).toBe('teste1')
            expect(response.data[3].name).toBe('teste4')
        } catch (e) {
            expect(e.status).toBe(200)
        }
    })

    it(' Error find', async () => {

        const mockFunc = jest.fn();
        mockFunc.mockReturnValue(Promise.reject({ status: 400, error: 'error' }));
        Request.getUsers = mockFunc.bind(Request)
        try {
            const response = await UserService.getNameEmailCompany()
            expect(response.status).not.toBe(200)
        } catch (e) {

            expect(e.status).toBe(400)
        }
    })
})

describe('get filter suite', () => {

    it(' sucess find', async () => {


        const body = [
            { address: { street: 'suite do casais', suite: "apt 15", city: 'sao ds' } },
            { address: { street: ' do casais', suite: "apt 15", city: 'sao ds' } },
            { address: { street: 'casais', suite: "apt 15", city: 'sao ds' } },
            { address: { street: 'casais', suite: "apt 15", city: 'sao ds' } },
            { address: { street: ' do casais', suite: "suite 15", city: 'sao ds' } },
            { address: { street: ' do casais', suite: "apt 15", city: 'sao ds' } },
            { address: { street: ' do casais', suite: "apt 15", city: 'sao suite' } },
        ]

        const mockFunc = jest.fn();
        mockFunc.mockReturnValue(body);
        Request.getUsers = mockFunc.bind(Request)
        try {
            const response = await UserService.filterSuite()
            expect(response.status).toBe(200)
            expect(response.data.length).toBe(3)
           
        } catch (e) {
            expect(e.status).toBe(200)
        }
    })

    it(' Error find', async () => {

        const mockFunc = jest.fn();
        mockFunc.mockReturnValue(Promise.reject({ status: 400, error: 'error' }));
        Request.getUsers = mockFunc.bind(Request)
        try {
            const response = await UserService.getNameEmailCompany()
            expect(response.status).not.toBe(200)
        } catch (e) {

            expect(e.status).toBe(400)
        }
    })
})