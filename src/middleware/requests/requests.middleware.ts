import Fetch from 'node-fetch'
import Constant from '../../constant'
class RequestMiddleware {

    async getUsers(): Promise<any> {
        try {
            const response = await Fetch(Constant.API_USERS, { method: "GET" })
            return Promise.resolve(response.json())
        } catch (error) {
            return Promise.reject(error.toString())
        }
    }

}

export default new RequestMiddleware() 