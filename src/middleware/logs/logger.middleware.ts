import Winston from 'winston'
import WinstonElasticsearch from 'winston-elasticsearch'
import { LogModel, LogLevel } from '../../models/log.model';
import Constant from '../../constant'

class LoggerMiddleware {
    private transportInfo = {
        level: 'info',
        clientOpts: { node: Constant.ELASTIC_NODE }
    };

    private transportError = {
        level: 'error',
        clientOpts: { node: Constant.ELASTIC_NODE }
    };


    protected createLog(log: LogModel): void {
        const logger = Winston.createLogger({
            transports: [
                new WinstonElasticsearch(this.transportInfo),
                new WinstonElasticsearch(this.transportError),
            ]
        })

        if (!Constant.PRODUCTION) {
            logger.add(new Winston.transports.Console({
                format: Winston.format.simple()
            }));
        }


        switch (log.level) {
            case LogLevel.ERROR: logger.error(log.message, log.fields); break;
            case LogLevel.INFO: logger.info(log.message, log.fields); break;
        }

    }

}


export default LoggerMiddleware