

export ={
    APP_PORT: process.env.APP_PORT ? process.env.APP_PORT : '8080',
    ELASTIC_NODE: process.env.ELASTIC_NODE ? process.env.ELASTIC_NODE : 'http://localhost:9200',
    PRODUCTION: process.env.PRODUCTION_APP ? process.env.PRODUCTION_APP : false,
    API_USERS: "https://jsonplaceholder.typicode.com/users"
}