import Request from '../middleware/requests/requests.middleware'
import { ResponseModel } from '../models/response.model'

class UserService {

    async  getWebsites(): Promise<ResponseModel> {
        try {
            const user = await Request.getUsers()
            const response: ResponseModel = {
                data: user.map((data: any) => data.website),
                status: 200,
                message: "Success find web sites"
            }
            return Promise.resolve(response)
        } catch (e) {
            throw { error: e.toString(), message: 'error to get websites', status: 400 }
        }
    }


    async  getNameEmailCompany(): Promise<ResponseModel> {
        try {
            const user = await Request.getUsers()

            const dataFilter = user.sort((a: any, b: any) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()))
                .map((data: any) => ({ name: data.name, email: data.email, company: data.company.name }))

            const response: ResponseModel = {
                data: dataFilter,
                status: 200,
                message: "Success find name email company"
            }
            return Promise.resolve(response)
        } catch (e) {
            throw { error: e.toString(), message: 'error to get email name company', status: 400 }
        }
    }

    async  filterSuite(): Promise<ResponseModel> {
        try {
            const user = await Request.getUsers()

            const dataFilter = user.filter((element: any) => {
                if (element.address.street.toLowerCase().includes('suite') || element.address.suite.toLowerCase().includes('suite') || element.address.city.toLowerCase().includes('suite')) {
                    return element
                }
            })

            const response: ResponseModel = {
                data: dataFilter,
                status: 200,
                message: "Success filter suite"
            }
            return Promise.resolve(response)
        } catch (e) {
            throw { error: e.toString(), message: 'error to filter suite', status: 400 }
        }
    }


}

export default new UserService()