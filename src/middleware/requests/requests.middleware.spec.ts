import RequestMiddleware from './requests.middleware'

import Fetch from 'node-fetch';

const { Response } = jest.requireActual('node-fetch');
jest.mock('node-fetch', () => jest.fn());


describe('test get data', () => {
    it('Success get data', async () => {


        const expectedResponse = { test: 'TEST' };
        (Fetch as jest.MockedFunction<typeof Fetch>).mockResolvedValueOnce(new Response(JSON.stringify(expectedResponse)));

        const users = await RequestMiddleware.getUsers()
        expect(users.test).toBe('TEST')

    })

    it('Error get data', async () => {


        const mockedFetchError = new Error('some error');
        (Fetch as jest.MockedFunction<typeof Fetch>).mockRejectedValueOnce(mockedFetchError);

        try {
            await RequestMiddleware.getUsers()
        } catch (error) {
            
            expect(error).toEqual('Error: some error');

        }


    })
})
